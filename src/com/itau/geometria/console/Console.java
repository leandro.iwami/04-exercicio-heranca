package com.itau.geometria.console;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.itau.geometria.formas.Forma;

public class Console {
	private static Scanner scanner = new Scanner(System.in);
	
	public static List<Double> lerEntrada(){
		List<Double> lados = new ArrayList<>();
		
		Double entradaDouble = -1.0;
		
		while(entradaDouble != 0.0) {
			imprimirMenu();
			entradaDouble = lerDouble();
			
			if(entradaDouble > 0) {
				lados.add(entradaDouble);
			}
		}
		
		return lados;
	}
	
	public static void imprimir(Forma forma) {
		System.out.println(forma);
	}
	
	private static void imprimirMenu() {
		System.out.println("Digite um número para o lado da forma ou digite 0 para sair");
	}
	
	private static double lerDouble() {
		//TODO: tratar exceção
		String entrada = scanner.nextLine();
		return Double.parseDouble(entrada);
	}
}
