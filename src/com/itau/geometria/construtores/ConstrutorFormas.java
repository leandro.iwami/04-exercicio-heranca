package com.itau.geometria.construtores;

import java.util.List;

import com.itau.geometria.excecoes.NaoImplementadoException;
import com.itau.geometria.excecoes.TrianguloInvalidoException;
import com.itau.geometria.formas.Circulo;
import com.itau.geometria.formas.Forma;
import com.itau.geometria.formas.Retangulo;
import com.itau.geometria.formas.Triangulo;

public class ConstrutorFormas {
	public static Forma construir(List<Double> lados) throws TrianguloInvalidoException, NaoImplementadoException {
		if(lados.size() == 1) {
			return new Circulo(lados.get(0));
		}
		
		if(lados.size() == 2) {
			return new Retangulo(lados.get(0), lados.get(1));
		}
		
		if(lados.size() == 3) {
			return new Triangulo(lados.get(0), lados.get(1), lados.get(2));
		}
		
		throw new NaoImplementadoException();
	}
}
