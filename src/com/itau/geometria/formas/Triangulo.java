package com.itau.geometria.formas;

import com.itau.geometria.excecoes.TrianguloInvalidoException;

public class Triangulo extends Forma {
	private double ladoA;
	private double ladoB;
	private double ladoC;
	
	public Triangulo(double ladoA, double ladoB, double ladoC) throws TrianguloInvalidoException {
		this.ladoA = ladoA;
		this.ladoB = ladoB;
		this.ladoC = ladoC;
		
		if(! eValido()){
			throw new TrianguloInvalidoException();
		}
	}
	
	public double getLadoA() {
		return ladoA;
	}
	
	public double getLadoB() {
		return ladoB;
	}
	
	public double getLadoC() {
		return ladoC;
	}
	
	private boolean eValido() {
		return (ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA;
	}

	@Override
	public double calcularArea() {
		double s = (ladoA + ladoB + ladoC) / 2;       
		return Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
	}

}
