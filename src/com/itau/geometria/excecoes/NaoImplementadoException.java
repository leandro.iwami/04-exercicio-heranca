package com.itau.geometria.excecoes;

public class NaoImplementadoException extends Exception{
	public NaoImplementadoException() {
		super("Formas com mais de 3 lados não foram implementadas. Volte mais tarde.");
	}
}
