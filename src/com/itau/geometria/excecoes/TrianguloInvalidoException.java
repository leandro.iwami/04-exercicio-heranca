package com.itau.geometria.excecoes;

public class TrianguloInvalidoException extends Exception {
	public TrianguloInvalidoException() {
		super("Triângulo Inválido");
	}
}
